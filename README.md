# library

## Linear algebra

- [A tutorial on principal component analysis](https://arxiv.org/abs/1404.1100)

## Convolutional neural networks

- [CS231n Convolutional Neural Networks for Visual Recognition](https://cs231n.github.io/) (Standford)

## Generative adversarial neural networks

- [GAN Dissection: Visualizing and Understanding Generative Adversarial Networks](https://arxiv.org/abs/1811.10597)
- [Generating and Tuning Realistic Artificial Faces](https://www.lyrn.ai/2018/12/26/a-style-based-generator-architecture-for-generative-adversarial-networks/)

## Books

- hktxt's [bookshelf](https://github.com/hktxt/bookshelf)

## Misc

- [ColorBrewer](http://colorbrewer2.org/#type=sequential&scheme=YlGnBu&n=3): color advice for maps
- [Snip](https://mathpix.com/): converting math screenshots to Latex code
- [RISE](https://github.com/damianavila/RISE): "Live" Reveal.js Jupyter/IPython Slideshow Extension

